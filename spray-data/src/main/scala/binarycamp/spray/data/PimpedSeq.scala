package binarycamp.spray.data

final class PimpedSeq[T](val seq: Seq[T]) extends AnyVal {
  def page(request: PageRequest, defaultPageSize: Int = 10): Page[T] = {
    val size = request.size.getOrElse(defaultPageSize)
    val items = seq.slice(request.index * size, (request.index + 1) * size)
    Page(items, request.index, size, seq.size, request.sort, request.filter)
  }
}
