package binarycamp.spray.common

import spray.http.StatusCode
import spray.httpx.marshalling.ToResponseMarshaller

trait ResultMarshallers extends LowPriorityResultMarshallers {
  implicit def statusCodeAndResultMarshaller[R, S](implicit m: ToResponseMarshaller[Either[Error, (S, R)]],
                                                   s: S ⇒ StatusCode): ToResponseMarshaller[(S, Result[R])] =
    ToResponseMarshaller[(S, Result[R])] {
      case ((sc, result), ctx) ⇒ m(result.toEither.right.map(sc → _), ctx)
    }

  implicit def statusCodeAndResultLocMarshaller[R, S](implicit m: LocalizableMarshaller[Either[Error, (S, R)]],
                                                      s: S ⇒ StatusCode): LocalizableMarshaller[(S, Result[R])] =
    LocalizableMarshaller[(S, Result[R])] { languages ⇒
      ToResponseMarshaller {
        case ((sc, result), ctx) ⇒ m(languages)(result.toEither.right.map(sc → _), ctx)
      }
    }
}

trait LowPriorityResultMarshallers {
  implicit def resultMarshaller[R](implicit m: ToResponseMarshaller[Either[Error, R]]): ToResponseMarshaller[Result[R]] =
    ToResponseMarshaller[Result[R]] {
      case (result, ctx) ⇒ m(result.toEither, ctx)
    }

  implicit def resultLocalizableMarshaller[R](implicit m: LocalizableMarshaller[Either[Error, R]]): LocalizableMarshaller[Result[R]] =
    LocalizableMarshaller[Result[R]] { languages ⇒
      ToResponseMarshaller {
        case (result, ctx) ⇒ m(languages)(result.toEither, ctx)
      }
    }
}
