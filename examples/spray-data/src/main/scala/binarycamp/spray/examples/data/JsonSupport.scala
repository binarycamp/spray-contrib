package binarycamp.spray.examples.data

import binarycamp.spray.common.AdditionalMarshallers
import binarycamp.spray.data.DataSprayJsonFormats
import spray.httpx.SprayJsonSupport

trait JsonSupport
    extends DataSprayJsonFormats
    with SprayJsonSupport
    with AdditionalMarshallers {

  implicit val bookJsonFormat = jsonFormat3(Book)
  implicit val newBookJsonFormat = jsonFormat2(NewBook)
}
