package binarycamp.spray

import com.typesafe.config.Config
import scala.language.implicitConversions
import spray.http.Uri
import spray.http.Uri.{ Path, Query }

package object pimp {
  implicit def pimpConfig_(config: Config): PimpedConfig = new PimpedConfig(config)
  implicit def pimpMap[A, B](map: Map[A, B]): PimpedMap[A, B] = new PimpedMap(map)
  implicit def pimpSeq_[A](seq: Seq[A]): PimpedSeq[A] = new PimpedSeq[A](seq)
  implicit def pimpUri(uri: Uri): PimpedUri = new PimpedUri(uri)
  implicit def pimpPath(path: Path): PimpedPath = new PimpedPath(path)
  implicit def pimpQuery(query: Query): PimpedQuery = new PimpedQuery(query)
}
