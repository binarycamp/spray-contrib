package binarycamp.spray.pimp

import spray.http.Uri
import spray.http.Uri.Path._
import spray.http.Uri.{ Path, Query }

class PimpedUri(val uri: Uri) extends AnyVal {
  def /(path: Path): Uri = {
    require(uri.query.isEmpty)
    require(uri.fragment.isEmpty)
    uri.copy(path = uri.path ++ path)
  }

  def /(segment: String): Uri = {
    require(uri.query.isEmpty)
    require(uri.fragment.isEmpty)
    uri.copy(path = uri.path / segment)
  }

  def withQueryUpdated[T](t: T)(implicit f: T ⇒ Map[String, String]): Uri = uri.withQuery(uri.query.toMap ++ f(t))

  def withFragment(map: Map[String, String]): Uri = {
    val fragment = Query(map).toString()
    if (uri.path.isEmpty && uri.query.isEmpty) {
      uri.copy(path = SingleSlash, fragment = Some(fragment))
    } else {
      uri.copy(fragment = Some(fragment))
    }
  }
}
