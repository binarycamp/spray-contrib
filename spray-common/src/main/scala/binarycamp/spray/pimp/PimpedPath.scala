package binarycamp.spray.pimp

import shapeless.HNil
import spray.http.Uri.Path
import spray.routing.{ PathMatcher, PathMatcher0 }

class PimpedPath(val path: Path) extends AnyVal {
  def asMatcher: PathMatcher0 = PathMatcher(path, HNil)
}
