package binarycamp.spray.data

import org.scalatest.FlatSpec
import org.scalatest.Matchers._
import spray.http.Uri
import spray.json._

class HypermediaSprayJsonFormatsSpec extends FlatSpec with DataSprayJsonFormats {
  val sort = Sort("p1" -> Some(Order.Asc), "p2" -> Some(Order.Desc), "p3" -> None)
  val filter = Filter("expr")
  val page = Page(Seq("1", "2", "3"), 1, 3, 8, Some(sort), Some(filter))
  val pageWithLinks = page.asCollectionResource(Uri("http://localhost/items"))

  val pageJson = """|{
                    |  "items": ["1", "2", "3"],
                    |  "count": 8,
                    |  "@links": {
                    |    "self": {
                    |      "href": "http://localhost/items?page=1&size=3&sort=p1:asc,p2:desc,p3&filter=expr"
                    |    },
                    |    "prev": {
                    |      "href": "http://localhost/items?page=0&size=3&sort=p1:asc,p2:desc,p3&filter=expr"
                    |    },
                    |    "next": {
                    |      "href": "http://localhost/items?page=2&size=3&sort=p1:asc,p2:desc,p3&filter=expr"
                    |    }
                    |  }
                    |}""".stripMargin.parseJson

  "withHypermediaJsonFormat" should "convert a WithHypermedia[Page] to a JsObject" in {
    pageWithLinks.toJson should be(pageJson)
  }

  it should "convert a JsObject to a WithHypermedia[Page]" in {
    pageJson.convertTo[WithHypermedia[Page[String]]] should be(pageWithLinks)
  }
}
