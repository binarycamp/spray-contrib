package binarycamp.spray.common

import scala.concurrent.{ ExecutionContext, Future }
import scala.util.{ Failure, Success, Try }
import spray.http.StatusCodes._
import spray.http._
import spray.httpx.marshalling._

trait LocalizableMarshaller[-T] {
  def apply(languages: Seq[LanguageRange]): ToResponseMarshaller[T]
}

object LocalizableMarshaller
    extends StatusCodeLocalizableMarshallers
    with MetaLocalizableMarshallers
    with LowPriorityLocalizableMarshallers {

  def apply[T](f: Seq[LanguageRange] ⇒ ToResponseMarshaller[T]): LocalizableMarshaller[T] =
    new LocalizableMarshaller[T] {
      override def apply(languages: Seq[LanguageRange]): ToResponseMarshaller[T] = f(languages)
    }
}

trait StatusCodeLocalizableMarshallers extends LowPriorityStatusCodeLocalizableMarshallers {
  implicit def statusCodeAndOptionLocMarshaller[S, T](implicit m: LocalizableMarshaller[(S, T)],
                                                      s: S ⇒ StatusCode): LocalizableMarshaller[(S, Option[T])] =
    LocalizableMarshaller[(S, Option[T])] { languages ⇒
      ToResponseMarshaller {
        case ((sc, Some(t)), ctx) ⇒ m(languages)(sc → t, ctx)
        case ((_, None), ctx)     ⇒ ctx.marshalTo(HttpResponse(NotFound))
      }
    }

  implicit def statusCodeAndEitherLocMarshaller[S, A, B](implicit ma: LocalizableMarshaller[A],
                                                         mb: LocalizableMarshaller[(S, B)],
                                                         s: S ⇒ StatusCode): LocalizableMarshaller[(S, Either[A, B])] =
    LocalizableMarshaller[(S, Either[A, B])] { languages ⇒
      ToResponseMarshaller {
        case ((_, Left(a)), ctx)   ⇒ ma(languages)(a, ctx)
        case ((sc, Right(b)), ctx) ⇒ mb(languages)(sc → b, ctx)
      }
    }

  implicit def statusCodeAndFutureLocMarshaller[S, T](implicit m: LocalizableMarshaller[(S, T)],
                                                      ec: ExecutionContext,
                                                      s: S ⇒ StatusCode): LocalizableMarshaller[(S, Future[T])] =
    LocalizableMarshaller[(S, Future[T])] { languages ⇒
      ToResponseMarshaller {
        case ((sc, f), ctx) ⇒ f onComplete {
          case Success(v)     ⇒ m(languages)(sc → v, ctx)
          case Failure(error) ⇒ ctx.handleError(error)
        }
      }
    }

  implicit def statusCodeAndTryLocMarshaller[S, T](implicit m: LocalizableMarshaller[(S, T)],
                                                   s: S ⇒ StatusCode): LocalizableMarshaller[(S, Try[T])] =
    LocalizableMarshaller[(S, Try[T])] { languages ⇒
      ToResponseMarshaller {
        case ((sc, Success(v)), ctx) ⇒ m(languages)(sc → v, ctx)
        case ((_, Failure(t)), ctx)  ⇒ ctx.handleError(t)
      }
    }

  implicit def statusCodeTLocMarshaller[S, T](implicit m: ToResponseMarshaller[(S, T)],
                                              s: S ⇒ StatusCode): LocalizableMarshaller[(S, T)] =
    new LocalizableMarshaller[(S, T)] {
      override def apply(languages: Seq[LanguageRange]): ToResponseMarshaller[(S, T)] = m
    }
}

trait LowPriorityStatusCodeLocalizableMarshallers {
  implicit def statusCodeTLocMarshaller2[S, T](implicit m: LocalizableMarshaller[T],
                                               s: S ⇒ StatusCode): LocalizableMarshaller[(S, T)] =
    LocalizableMarshaller[(S, T)] { languages ⇒
      ToResponseMarshaller { (value, ctx) ⇒
        m(languages)(value._2, ctx.withResponseMapped { response ⇒
          response.copy(status = value._1)
        })
      }
    }

  implicit def statusCodeAndHeadersAndTLocMarshaller[S, T](implicit m: LocalizableMarshaller[T],
                                                           s: S ⇒ StatusCode): LocalizableMarshaller[(S, Seq[HttpHeader], T)] =
    LocalizableMarshaller[(S, Seq[HttpHeader], T)] { languages ⇒
      ToResponseMarshaller { (value, ctx) ⇒
        m(languages)(value._3, ctx.withResponseMapped { response ⇒
          response.copy(status = value._1, headers = (value._2 ++ response.headers).toList)
        })
      }
    }
}

trait MetaLocalizableMarshallers {
  implicit def optionMarshaller[T](implicit m: LocalizableMarshaller[T]): LocalizableMarshaller[Option[T]] =
    LocalizableMarshaller[Option[T]] { languages ⇒
      ToResponseMarshaller {
        case (Some(value), ctx) ⇒ m(languages)(value, ctx)
        case (None, ctx)        ⇒ ctx.marshalTo(HttpResponse(NotFound))
      }
    }

  implicit def eitherMarshaller[A, B](implicit ma: LocalizableMarshaller[A],
                                      mb: LocalizableMarshaller[B]): LocalizableMarshaller[Either[A, B]] =
    LocalizableMarshaller[Either[A, B]] { languages ⇒
      ToResponseMarshaller {
        case (Left(a), ctx)  ⇒ ma(languages)(a, ctx)
        case (Right(b), ctx) ⇒ mb(languages)(b, ctx)
      }
    }

  implicit def futureMarshaller[T](implicit m: LocalizableMarshaller[T],
                                   ec: ExecutionContext): LocalizableMarshaller[Future[T]] =
    LocalizableMarshaller[Future[T]] { languages ⇒
      ToResponseMarshaller { (value, ctx) ⇒
        value.onComplete {
          case Success(v)     ⇒ m(languages)(v, ctx)
          case Failure(error) ⇒ ctx.handleError(error)
        }
      }
    }

  implicit def tryMarshaller[T](implicit m: LocalizableMarshaller[T]): LocalizableMarshaller[Try[T]] =
    LocalizableMarshaller[Try[T]] { languages ⇒
      ToResponseMarshaller {
        case (Success(value), ctx) ⇒ m(languages)(value, ctx)
        case (Failure(t), ctx)     ⇒ ctx.handleError(t)
      }
    }
}

trait LowPriorityLocalizableMarshallers {
  implicit def defaultLocalizableMarshaller[T](implicit m: ToResponseMarshaller[T]): LocalizableMarshaller[T] =
    new LocalizableMarshaller[T] {
      override def apply(languages: Seq[LanguageRange]): ToResponseMarshaller[T] = m
    }
}
