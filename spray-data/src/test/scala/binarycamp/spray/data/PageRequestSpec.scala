package binarycamp.spray.data

import org.scalatest.FlatSpec
import org.scalatest.Matchers._

class PageRequestSpec extends FlatSpec {
  val sort = Sort("p1" -> Some(Order.Asc), "p2" -> Some(Order.Desc), "p3" -> None)
  val filter = Filter("expr")

  "PageRequest.apply(page)" should "build a PageRequest with the same index and size as the Page" in {
    val request = PageRequest.apply(Page(Seq("1", "2", "3"), 1, 4, 7))
    request should be(PageRequest(1, Some(4), None, None))
  }

  it should "build a PageRequest with the same index, size and sort as the Page" in {
    val request = PageRequest.apply(Page(Seq("1", "2", "3"), 1, 4, 7, sort))
    request should be(PageRequest(1, Some(4), Some(sort), None))
  }

  it should "build a PageRequest with the same index, size and filter as the Page" in {
    val request = PageRequest.apply(Page(Seq("1", "2", "3"), 1, 4, 7, filter))
    request should be(PageRequest(1, Some(4), None, Some(filter)))
  }

  it should "build a PageRequest with the same index, size, sort and filter as the Page" in {
    val request = PageRequest.apply(Page(Seq("1", "2", "3"), 1, 4, 7, sort, filter))
    request should be(PageRequest(1, Some(4), Some(sort), Some(filter)))
  }

  val firstPage = Page(Seq("1", "2", "3", "4"), 0, 4, 7, sort, filter)
  val lastPage = Page(Seq("1", "2", "3"), 1, 4, 7, sort, filter)

  "PageRequest.prev(page)" should "build a PageRequest with index decreased by 1 when the Page has a previous page" in {
    val request = PageRequest.apply(lastPage)
    PageRequest.prev(lastPage) should be(Some(request.copy(index = request.index - 1)))
  }

  it should "return None when the Page has no previous page" in {
    PageRequest.prev(firstPage) should be(None)
  }

  "PageRequest.next(page)" should "build a PageRequest with index increased by 1 when the Page has a next page" in {
    val request = PageRequest.apply(firstPage)
    PageRequest.next(firstPage) should be(Some(request.copy(index = request.index + 1)))
  }

  it should "return None when the Page has no next page" in {
    PageRequest.next(lastPage) should be(None)
  }
}
